const path = require("path");
const fs = require("fs");

const dirPath = "./RandomJSONFiles";
const numberOfFiles = 4;

const createDirectory = () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(dirPath, (error) => {
      if (error) {
        reject(`Error occurred while creating the directory: ${error}`);
      } else {
        resolve(`Directory created successfully`);
      }
    });
  });
};

const createJSONFiles = () => {
  const promises = [];

  const fileContent = "Solving Drills";

  for (let index = 0; index < numberOfFiles; index++) {
    const filePath = path.join(__dirname, dirPath, `RandomJson${index}.json`);

    promises.push(
      new Promise((resolve, reject) => {
        fs.writeFile(filePath, fileContent, (error) => {
          if (error) {
            reject(`Error creating ${filePath} file: ${error}`);
          } else {
            resolve(`${filePath} created successfully`);
          }
        });
      })
    );
  }
  return promises;
};

const deleteJSONFiles = () => {
  let numberOfFilesDeleted = 0;

  return new Promise((resolve, reject) => {
    fs.readdir(dirPath, (error, files) => {
      if (error) {
        console.log(error);
        reject(`Error reading directory: ${error}`);
        return;
      }

      files.forEach((file) => {
        const filePath = path.join(dirPath, file);

        fs.unlink(filePath, (error) => {
          if (error) {
            console.log(`Error deleting file: ${error}`);
          } else {
            console.log(`Deleted file: ${filePath}`);
            numberOfFilesDeleted++;

            if (numberOfFiles === numberOfFilesDeleted) {
              resolve(`All files deleted successfully`);
            }
          }
        });
      });
    });
  });
};

createDirectory()
  .then((message) => {
    console.log(message);

    const composedPromises = createJSONFiles();

    Promise.all(composedPromises)
      .then((results) => {
        results.forEach((result) => {
          console.log(result);
        });

        deleteJSONFiles()
          .then((message) => {
            console.log(message);
          })
          .catch((message) => {
            console.log(message);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  })
  .catch((message) => {
    console.log(message);
  });
