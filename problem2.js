const fs = require('fs')
const path = require('path')

let ipsumFileContent = ''

const readIpsumFile = () => {

    const ipsumFilePath = './lipsum_1.txt'

    return new Promise((resolve, reject) => {

        fs.readFile(ipsumFilePath, 'utf8', (error, content) => {
            if (error) {
                reject(error)
            }
            else {
                ipsumFileContent = content;
                resolve(`File read successfully`)
            }
        })
    })
}

const convertIpsumToUpperCase = () => {

    return new Promise((resolve, reject) => {

        ipsumFileContentInUpperCase = ipsumFileContent.toUpperCase();

        fs.writeFile('./UpperCaseIpsum.txt', ipsumFileContentInUpperCase, (error) => {
            if (error) {
                reject(error)
            }
            else {
                fs.appendFile('./filenames.txt', 'UpperCaseIpsum.txt\n', (error) => {
                    if (error) {
                        console.log(error)
                    }
                })
                resolve(`File converted and written to the file successfully`)
            }
        })
    })
}

const convertToLowerCaseAndSplit = () => {

    return new Promise((resolve, reject) => {

        fs.readFile('./UpperCaseIpsum.txt', 'utf8', (error, content) => {
            if (error) {
                console.log(`Error reading Ipsum Upper Case File`)
                return;
            }


            let ipsumFileContentInLowerCase = content.toLowerCase();

            let senetencesArrayOfIpsumInLowerCase = ipsumFileContentInLowerCase.split(".")

            senetencesArrayOfIpsumInLowerCase.forEach(eachSentence => {
                fs.appendFile('./sentencesInLowerCase.txt', eachSentence + '\n', (error) => {
                    if (error) {
                        reject(error)
                    }
                })
            });

            fs.appendFile('./filenames.txt', 'sentencesInLowerCase.txt\n', (error) => {
                if (error) {
                    console.log(`Error while adding name of the file in filenames.txt`)
                }
                else {
                    resolve(`File converted to Lower Case and written into a new file successfully`)
                }
            })
        })
    })
}

const sortSentencesFile = () => {

    return new Promise((resolve, reject) => {

        fs.readFile('./sentencesInLowerCase.txt', 'utf8', (error, content) => {
            if (error) {
                console.log(error)
            }

            let lines = content.split('\n')

            lines.sort((a, b) => {
                return a.toLowerCase().localeCompare(b.toLowerCase())
            });

            lines.forEach(eachSentence => {
                fs.appendFile('./sortedSentences.txt', eachSentence + '\n', (error) => {
                    if (error) {
                        console.log(`Error while writing sorted sentences into a new file`)
                    }
                })
            });

            fs.appendFile('./filenames.txt', 'sortedSentences.txt\n', (error) => {
                if (error) {
                    console.log(`Error while adding name of the file in filenames.txt`)
                }
                else {
                    resolve(`Sentences sorted and written into a new file successfully`)
                }
            })
        })
    })
}

const readAndDeleteFileNames = () => {

    return new Promise((resolve, reject) => {

        fs.readFile('./filenames.txt', 'utf8', (error, nameOfFiles) => {
            if (error) {
                reject(error)
                return;
            }

            nameOfFiles = nameOfFiles.split('\n')
            nameOfFiles.forEach((file) => {

                const filePath = path.join(__dirname, './Async_With_Promises', file)

                fs.unlink(filePath, (error) => {
                    if (error) {
                        console.log(`Error deleting file: ${error}`)
                        reject(error)
                    }
                })
            })
        })

        resolve(`All file names deleted successfully`)
    })
}

readIpsumFile().then((message) => {
    console.log(message)

    convertIpsumToUpperCase().then((message) => {
        console.log(message)

        convertToLowerCaseAndSplit().then((message) => {
            console.log(message)

            sortSentencesFile().then((message) => {
                console.log(message)

                readAndDeleteFileNames().then((message) => {
                    console.log(message)
                }).catch((message) => {
                    console.log(message)
                })
            }).catch((message) => {
                console.log(message)
            })
        }).catch((message) => {
            console.log(message)
        })
    }).catch((message) => {
        console.log(message)
    })
}).catch((message) => {
    console.log(message)
})